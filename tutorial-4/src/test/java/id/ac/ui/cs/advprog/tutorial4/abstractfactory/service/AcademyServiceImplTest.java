package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository  = new AcademyRepository();

    @InjectMocks
    private AcademyService academyService = new AcademyServiceImpl(academyRepository);

    // TODO create tests
    @Test
    public void testGetKnightAcademies(){
        assertEquals(2,academyService.getKnightAcademies().size());
    }

    @Test
    public void testProduceKnightAndGetKnight(){
        academyService.produceKnight("Drangleic","majestic");
        Knight knight = academyService.getKnight();
        assertNotNull(knight);
        assertEquals("Majestic Knight",knight.getName());
    }
}
