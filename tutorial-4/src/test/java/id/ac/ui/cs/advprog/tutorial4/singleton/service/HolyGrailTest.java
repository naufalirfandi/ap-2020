package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {
    private HolyGrail holyGrail;

    @BeforeEach
    public void setUp(){
        holyGrail = new HolyGrail();
    }

    @Test
    public void testGetHolyWish(){
        assertNotNull(holyGrail.getHolyWish());
    }

    @Test
    public void testMakeAWishRun(){
        holyGrail = spy(holyGrail);
        holyGrail.makeAWish("");

        verify(holyGrail, atLeastOnce()).makeAWish("");

    }

    // TODO create tests
}
