package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class LordranAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        drangleicAcademy = new DrangleicAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertNotNull(majesticKnight);
        assertNotNull(metalClusterKnight);
        assertNotNull(syntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals("Majestic Knight", majesticKnight.getName());
        assertEquals("Metal Cluster Knight", metalClusterKnight.getName());
        assertEquals("Synthetic Knight", syntheticKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertNotNull(majesticKnight.getArmor());
        assertNotNull(majesticKnight.getWeapon());
        assertNull(majesticKnight.getSkill());

        assertNotNull(metalClusterKnight.getArmor());
        assertNotNull(metalClusterKnight.getSkill());
        assertNull(metalClusterKnight.getWeapon());

        assertNotNull(syntheticKnight.getSkill());
        assertNotNull(syntheticKnight.getWeapon());
        assertNull(syntheticKnight.getArmor());
    }
}
