package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        Member huda = new PremiumMember("Huda", "Programmer");
        guild.addMember(guildMaster, huda);
        Member huda2 = guild.getMember("Huda", "Programmer");
        int tmp = guildMaster.getChildMembers().size();
        assertEquals(1,tmp);
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        Member huda = new PremiumMember("Huda", "Programmer");
        guild.addMember(guildMaster, huda);
        Member huda2 = guild.getMember("Huda", "Programmer");
        assertEquals(huda,huda2);
        guild.removeMember(guildMaster, huda);
        int tmp = guildMaster.getChildMembers().size();
        assertEquals(0,tmp);
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member huda = new PremiumMember("Huda", "Programmer");
        guild.addMember(guildMaster, huda);
        Member huda2 = guild.getMember("Huda", "Programmer");
        assertEquals(huda,huda2);
    }
}
