package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShieldTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Shield();
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String name = weapon.getName();
        assertEquals("shield", name);
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String desc = weapon.getDescription();
        assertEquals("can protect you", desc);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int value = weapon.getWeaponValue();
        assertEquals(10, value);
    }
}
