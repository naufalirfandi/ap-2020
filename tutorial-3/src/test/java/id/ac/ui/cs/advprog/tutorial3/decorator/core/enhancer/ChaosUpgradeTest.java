package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;


import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Gun;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChaosUpgradeTest {

    private ChaosUpgrade chaosUpgrade;
    @BeforeEach
    public void setUp(){
        chaosUpgrade = new ChaosUpgrade(new Gun());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String name = chaosUpgrade.getName();
        assertEquals("Gun", name);
    }

    @Test
    public void testGetMethodWeaponDescription(){
        //TODO: Complete me
        String desc = chaosUpgrade.getDescription();
        assertEquals("can shoot", desc);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int nilai = chaosUpgrade.getWeaponValue();
        assertTrue(70 <= nilai);
        assertTrue(75 >= nilai);
    }

}
