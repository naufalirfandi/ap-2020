package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        String name = member.getName();
        assertEquals("Aqua", name);
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        String role = member.getRole();
        assertEquals("Goddess", role);
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member dummy = new OrdinaryMember("huda", "programmer");
        member.addChildMember(dummy);
        int anak = member.getChildMembers().size();
        assertEquals(1, anak);
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member dummy = new OrdinaryMember("huda", "programmer");
        member.addChildMember(dummy);
        int anak = member.getChildMembers().size();
        assertEquals(1, anak);
        member.removeChildMember(dummy);
        int anak2 = member.getChildMembers().size();
        assertEquals(0,anak2);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member dummy = new OrdinaryMember("huda", "programmer");
        Member dummy1 = new OrdinaryMember("huda1", "programmer");
        Member dummy2 = new OrdinaryMember("huda2", "programmer");
        Member dummy3 = new OrdinaryMember("huda3", "programmer");
        member.addChildMember(dummy);
        member.addChildMember(dummy1);
        member.addChildMember(dummy2);
        member.addChildMember(dummy3);
        int anak = member.getChildMembers().size();
        assertEquals(3,anak);
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member master = new PremiumMember("hudaMaster", "Master");
        Member dummy = new OrdinaryMember("huda", "programmer");
        Member dummy1 = new OrdinaryMember("huda1", "programmer");
        Member dummy2 = new OrdinaryMember("huda2", "programmer");
        Member dummy3 = new OrdinaryMember("huda3", "programmer");
        master.addChildMember(dummy);
        master.addChildMember(dummy1);
        master.addChildMember(dummy2);
        master.addChildMember(dummy3);
        int anak = master.getChildMembers().size();
        assertEquals(4,anak);
    }
}
