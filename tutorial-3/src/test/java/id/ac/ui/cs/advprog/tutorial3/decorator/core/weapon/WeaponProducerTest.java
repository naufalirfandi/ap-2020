package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class WeaponProducerTest {

    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Weapon weapon4;

    @Test
    public void testCreateWeaponEnhancer() {
        weapon1 = WeaponProducer.WEAPON_GUN.createWeaponEnhancer();
        weapon2 = WeaponProducer.WEAPON_LONGBOW.createWeaponEnhancer();
        weapon3 = WeaponProducer.WEAPON_SHIELD.createWeaponEnhancer();
        weapon4 = WeaponProducer.WEAPON_SWORD.createWeaponEnhancer();
        //TODO: Complete me
        int data = weapon1.getWeaponValue();
        assertEquals(20,data);
        int data1 = weapon2.getWeaponValue();
        assertEquals(15, data1);
        int data2 = weapon3.getWeaponValue();
        assertEquals(10, data2);
        int data3 = weapon4.getWeaponValue();
        assertEquals(25,data3);
    }
}
