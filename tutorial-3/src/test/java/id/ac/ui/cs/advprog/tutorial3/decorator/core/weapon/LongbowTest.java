package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LongbowTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Longbow();
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String name = weapon.getName();
        assertEquals("longbow", name);
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String desc = weapon.getDescription();
        assertEquals("shoot arrow", desc);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int value = weapon.getWeaponValue();
        assertEquals(15, value);
    }
}
