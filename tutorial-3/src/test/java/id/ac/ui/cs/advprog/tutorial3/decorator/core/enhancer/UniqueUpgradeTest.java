package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UniqueUpgradeTest {

    private UniqueUpgrade uniqueUpgrade;

    @BeforeEach
    public void setUp(){
        uniqueUpgrade = new UniqueUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String name = uniqueUpgrade.getName();
        assertEquals("shield", name);
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String desc = uniqueUpgrade.getDescription();
        assertEquals("can protect you", desc);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int value = uniqueUpgrade.getWeaponValue();
        assertTrue(20 <= value);
        assertTrue(25 >= value);
    }
}
