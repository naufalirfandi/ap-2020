package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;



public class EnhancerDecoratorTest {

    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Weapon weapon4;
    Weapon weapon5;

    @BeforeEach
    public void setUp(){
        weapon1 = new Shield();
        weapon2 = new Shield();
        weapon3 = new Shield();
        weapon4 = new Shield();
        weapon5 = new Shield();
    }
    @Test
    public void testAddWeaponEnhancement(){

        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(weapon1);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon2);
        weapon3 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon3);
        weapon4 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon4);
        weapon5 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon5);

        //TODO: Complete me
        int data1 = weapon1.getWeaponValue();
        assertTrue(60<=data1);
        assertTrue(65>=data1);
        int data2 = weapon2.getWeaponValue();
        assertTrue(25<=data2);
        assertTrue(30>=data2);
        int data3 = weapon3.getWeaponValue();
        assertTrue(11<=data3);
        assertTrue(15>=data3);
        int data4 = weapon4.getWeaponValue();
        assertTrue(15<=data4);
        assertTrue(20>=data4);
        int data5 = weapon5.getWeaponValue();
        assertTrue(20<=data5);
        assertTrue(25>=data5);
    }

}
