package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
        //TODO: Complete me
    private String name;
    private String role;
    private List<Member> childMembers;
    public PremiumMember(String name, String position){
        this.name = name;
        this.role = position;
        this.childMembers = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public List<Member> getChildMembers() {
        return childMembers;
    }

    @Override
    public void addChildMember(Member member) {
        if(childMembers.size() < 3 || role.equals("Master")) {
            childMembers.add(member);
        }
    }

    @Override
    public void removeChildMember(Member member) {
        childMembers.remove(member);
    }
}
