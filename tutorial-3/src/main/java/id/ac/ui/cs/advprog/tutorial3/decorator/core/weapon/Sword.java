package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {
        //TODO: Complete me
    public Sword(){
        this.weaponName = "sword";
        this.weaponValue = 25;
        this.weaponDescription = "a sharp weapon";
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getDescription() {
        return super.getDescription();
    }

    @Override
    public int getWeaponValue() {
        return super.getWeaponValue();
    }
}
