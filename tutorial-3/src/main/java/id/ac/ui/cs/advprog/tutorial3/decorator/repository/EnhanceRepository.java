package id.ac.ui.cs.advprog.tutorial3.decorator.repository;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer.EnhancerDecorator;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class EnhanceRepository {

    public void enhanceToAllWeapons(ArrayList<Weapon> weapons){

        for (Weapon weapon: weapons) {
            int index = 0;
            switch (weapon.getName()) {
                case "Gun":
                    Weapon gun;
                    gun = WeaponProducer.WEAPON_GUN.createWeaponEnhancer();
                    gun = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(gun);
                    gun = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(gun);
                    index = weapons.indexOf(weapon);
                    weapons.set(index,gun);
                    break;

                //TODO: Complete me
                case "longbow":
                    Weapon longbow;
                    longbow = WeaponProducer.WEAPON_LONGBOW.createWeaponEnhancer();
                    longbow = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(longbow);
                    longbow = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(longbow);
                    longbow = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(longbow);
                    index = weapons.indexOf(weapon);
                    weapons.set(index,longbow);
                    break;

                case "shield":
                    Weapon shield;
                    shield = WeaponProducer.WEAPON_SHIELD.createWeaponEnhancer();
                    shield = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(shield);
                    shield = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(shield);
                    index = weapons.indexOf(weapon);
                    weapons.set(index, shield);
                    break;

                case "sword":
                    Weapon sword;
                    sword = WeaponProducer.WEAPON_SWORD.createWeaponEnhancer();
                    sword = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(sword);
                    sword = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(sword);
                    index = weapons.indexOf(weapon);
                    weapons.set(index, sword);
                    break;
            }
        }
    }
}
