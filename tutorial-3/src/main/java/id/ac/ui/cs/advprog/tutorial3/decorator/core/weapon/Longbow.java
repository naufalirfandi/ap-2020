package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
        //TODO: Complete me
    public Longbow(){
        this.weaponName = "longbow";
        this.weaponValue = 15;
        this.weaponDescription = "shoot arrow";
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getDescription() {
        return super.getDescription();
    }

    @Override
    public int getWeaponValue() {
        return super.getWeaponValue();
    }
}
