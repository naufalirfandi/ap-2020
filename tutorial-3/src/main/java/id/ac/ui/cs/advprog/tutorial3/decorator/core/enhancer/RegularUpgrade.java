package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        Random r = new Random();
        int plus = r.nextInt(5);
        if(plus == 0) {
            return weapon.getWeaponValue() + 1;
        }
        else{
            return weapon.getWeaponValue() + plus;
        }
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getDescription();
    }
}
