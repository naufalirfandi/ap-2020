package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    protected ArrayList<Spell> chainSpell;
    public ChainSpell(ArrayList<Spell> chainSpell){
        this.chainSpell = chainSpell;
    }

    @Override
    public void cast() {
        for(int i = 0; i<chainSpell.size(); i++){
            chainSpell.get(i).cast();
        }
    }

    @Override
    public void undo() {
        for(int i = chainSpell.size()-1; i>=0; i--){
            chainSpell.get(i).cast();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
