package id.ac.ui.cs.advprog.tutorial5.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class SoulTest {
    private Soul soul;

    @BeforeEach
    public void setUp(){
        soul = new Soul("upi", 60, "F", "menikung");
        soul.setId(1);
    }

    @Test
    void getGender(){
        assertEquals("F", soul.getGender());
    }
    @Test
    void setGender(){
        soul.setGender("M");
        assertEquals("M", soul.getGender());
    }
    @Test
    void getName(){
        assertEquals("upi", soul.getName());
    }
    @Test
    void setName(){
        soul.setName("ariel");
        assertEquals("ariel", soul.getName());
    }
    @Test
    void getAge(){
        assertEquals(60, soul.getAge());
    }
    @Test
    void setAge(){
        soul.setAge(19);
        assertEquals(19, soul.getAge());
    }
    @Test
    void getOccupation(){
        assertEquals("menikung", soul.getOccupation());
    }
    @Test
    void setOccupation(){
        soul.setOccupation("menusuk");
        assertEquals("menusuk", soul.getOccupation());
    }
    @Test
    void getId(){
        assertEquals(1, soul.getId());
    }
    @Test
    void setId(){
        soul.setId(2);
        assertEquals(2, soul.getId());
    }
}
